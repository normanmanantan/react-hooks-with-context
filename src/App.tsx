import React, { ReactElement, useContext } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import InstallPage from './components/Pages/InstallPage';
import QRScanner from './components/Pages/QRScanner';
import { GlobalContext } from './context';
import './App.scss';
  
export default function App(): ReactElement {
  const context = useContext(GlobalContext);
  const theme = context.theme.theme;
  const className = `app ${theme}`;

  return (
    <div className={className}> 
      <div className="theme-selector">
        <button onClick={context.SetDarkTheme}>
          Dark
        </button>
        <button onClick={context.SetLightTheme}>
          Light
        </button>
      </div>
      <Router>
        <Switch>
          <Route path="/scanner">
            <QRScanner />
          </Route>
          <Route path="/">
            <InstallPage />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}
