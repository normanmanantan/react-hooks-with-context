import React, { ReactElement, ReactNode } from 'react';
import './index.scss';

type ButtonType = 'button'|'submit';

interface IProps {
  className?: string;
  onClick?: () => void;
  type?: ButtonType;
  children: ReactNode;
};

export default function Button(props: IProps): ReactElement {
  const { className, onClick, type, children } = props;
  const buttonType = type ? type : 'button';
  
  return (
    <button className={className} onClick={onClick} type={buttonType}>
      {children}
    </button>
  );
}