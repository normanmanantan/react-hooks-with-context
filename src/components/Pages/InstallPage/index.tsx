import React, { ReactElement, useEffect } from 'react';
import InstallPageTemplate from 'src/components/Templates/InstallPageTemplate';
import { BeforeInstallPromptEvent } from '@models/index';

import logo from 'src/assets/logo/logo.svg';

let defferedPrompt: BeforeInstallPromptEvent|null = null;

export default function InstallPage(): ReactElement {
  const [isVisible, setVisibleState] = React.useState(true);
  const [isInstalled, setInstall] = React.useState(false);

  const hide = () => {
    const verify = window.confirm('Warning: You will not be able to access full functionality of this app without adding it to your home screen. Do you want to proceed?');

    if (verify) {
      setVisibleState(false);
    }
  }

  useEffect(
    () => {
      const ready = (e: BeforeInstallPromptEvent) => { console.log('ready');
        defferedPrompt = e;
        setVisibleState(true);
      };

      const appInstalled = (e: any) => { console.log('appInstalled', e);
        setVisibleState(false);
      };

      const trackPWA = (e: Event) => {
        if (('standalone' in window.navigator) && (window.navigator['standalone'])) {
          setInstall(true);
          setVisibleState(false);
        }
        else if (matchMedia('(display-mode: standalone)').matches) {
          setInstall(true);
          setVisibleState(false);
        }
        else {
          setInstall(false);
          setVisibleState(true);
        }
      }
  
      window.addEventListener('beforeinstallprompt', ready);
      window.addEventListener('appinstalled', appInstalled);
      window.addEventListener('load', trackPWA);
      
      return () => {
        window.removeEventListener('beforeinstallprompt', ready);
        window.removeEventListener('appinstalled', appInstalled);
        window.removeEventListener('load', trackPWA);
      };
    },
    []
  );

  const installApp = (): void => {
    const promptEvent = defferedPrompt;

    if (!promptEvent) {
      return;
    }

    promptEvent.prompt();
    promptEvent.userChoice.then((result) => {
      if (result.outcome === 'accepted') {
        setInstall(true);
      }
      else {
        setInstall(false);
      }
    });
  }

  return (
    <InstallPageTemplate 
      installApp={installApp}
      hide={hide}
      isVisible={isVisible}
      isInstalled={isInstalled}
      logo={logo}
      label="Progressive Web App Demo" />
  )
}