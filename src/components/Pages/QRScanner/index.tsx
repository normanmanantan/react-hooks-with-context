import React, { ReactElement, ReactNode, useState } from 'react';
import QrReader from 'react-qr-reader';
import BasicPageTemplate from '../../Templates/BasicPageTemplate'; 
import Button from '../../Atoms/Button';
import './index.scss';

export default function QRScanner(): ReactElement {
  const [result, setResult] = useState<string|boolean>(false);
 
  const handleScan = (data) => {
    if (data) {
      setResult(data)
    }
  }

  const handleError = (err) => {
    console.error(err)
  }

  const renderScannerOrResult = (): ReactNode => {
    if (result) {
      return (
        <div className="result-container">
          <p>{result}</p>
          <Button className="btn-primary" onClick={() => setResult(false)}>
            Scan Again
          </Button>
        </div>
        
      )
    }

    return (
      <QrReader
          delay={300}
          onError={handleError}
          onScan={handleScan}
          className="qr-capturer"
        />
    )
  }

  return (
    <BasicPageTemplate className="centered fullpage">
      <div className="wrapper">
        {renderScannerOrResult()}
      </div>
    </BasicPageTemplate>
  )
}