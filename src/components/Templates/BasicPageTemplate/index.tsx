import React, { ReactElement, ReactNode } from 'react';
import clsx from 'clsx';
import './index.scss';

interface IProps {
  children?: ReactNode;
  className?: string;
}

export default function InstallPageTemplate(props: IProps): ReactElement {
  const { children, className } = props;

  return (
    <div className={clsx('page-wrapper', className)}>
      {children}
    </div>
  )
}