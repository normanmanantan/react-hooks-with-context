import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';
import Button from 'src/components/Atoms/Button';
import BasicPageTemplate from '../BasicPageTemplate';
import './index.scss';

interface IProps {
  installApp: () => void;
  hide: () => void;
  logo: string;
  label: string;
  isVisible: boolean;
  isInstalled: boolean;
}

export default function InstallPageTemplate(props: IProps): ReactElement {
  const { installApp, hide, logo, label, isVisible, isInstalled } = props;

  const renderInstallButton = () => {
    if (!isVisible && isInstalled) {
      return (
        <div className="button-container">
          <Link to="/scanner">
            <Button className="btn-primary">
              Scan QR Code
            </Button>
          </Link>
        </div>
      );
    }

    else if (!isVisible && !isInstalled) {
      return (
        <div className="button-container">
          <p>You need to install the app. Please refresh the page to show the Install button again.</p>
        </div>
      );
    }

    return (
      <div id="installContainer" className="button-container">
        <Button onClick={installApp} className="btn-primary">
          Install to Home Screen
        </Button>
        <Button onClick={hide} className="btn-danger">
          Cancel Installation
        </Button>
      </div>
    );
  }

  return (
    <BasicPageTemplate className="centered fullpage">
      <div className="wrapper">
        <header className="app-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>{label}</p>
          {renderInstallButton()}
        </header>
      </div>
    </BasicPageTemplate>
  )
}