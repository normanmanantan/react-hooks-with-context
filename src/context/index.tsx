import React, { createContext, useReducer } from 'react';
import { LanguageReducer, GetLanguageAction, SetLanguageAction, languageInitialState } from './language';
import { ThemeReducer, GetThemeAction, SetDarkThemeAction, SetLightThemeAction, themeInitialState } from './theme';

interface State {
  language: {
    lang: string;
  };
  theme: {
    theme: string;
  };
  GetLanguage?: () => void;
  SetLanguage?: (payload: string) => void;
  GetTheme?: () => void;
  SetDarkTheme?: () => void;
  SetLightTheme?: () => void;
}

const initialState: State = {
  language: languageInitialState,
  theme: themeInitialState
};

export const GlobalContext = createContext(initialState);
export const GlobalProvider = ({ children }) => {
  const [langState, langDispatch] = useReducer(LanguageReducer, languageInitialState);
  const [themeState, themeDispatch] = useReducer(ThemeReducer, themeInitialState);

  const GetLanguage = () => langDispatch(new GetLanguageAction());
  const SetLanguage = (payload: string) => langDispatch(new SetLanguageAction(payload));

  const GetTheme = () => themeDispatch(new GetThemeAction());
  const SetDarkTheme = () => themeDispatch(new SetDarkThemeAction());
  const SetLightTheme = () => themeDispatch(new SetLightThemeAction());

  const languageProvider =  { language: langState, GetLanguage, SetLanguage };
  const themeProvider = { theme: themeState, GetTheme, SetDarkTheme, SetLightTheme };

  return (
    <GlobalContext.Provider value={{...languageProvider, ...themeProvider}}>
      {children}
    </GlobalContext.Provider>
  );
}