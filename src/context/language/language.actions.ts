export enum LanguageActionTypes {
  GetLanguage = '[Lang] Get Language',
  SetLanguage = '[Lang] Set Language'
};

export class GetLanguageAction {
  readonly type = LanguageActionTypes.GetLanguage;
}

export class SetLanguageAction {
  readonly type = LanguageActionTypes.SetLanguage;
  constructor(public lang: string) {}
}

export type LanguageAction = GetLanguageAction | SetLanguageAction;