import { LanguageActionTypes, LanguageAction } from './language.actions';

export interface ILanguageState {
  lang: string;
};

export const languageInitialState: ILanguageState = {
  lang: 'en'
};

export const LanguageReducer = (state = languageInitialState, action: LanguageAction) => {
  switch (action.type) {
    case LanguageActionTypes.GetLanguage:
      return {
        ...state
      };
    case LanguageActionTypes.SetLanguage:
      return {
        ...state,
        lang: action.lang
      };
    default:
      return state;
  }
}