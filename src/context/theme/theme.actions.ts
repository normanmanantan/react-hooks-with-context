export enum ThemeActionTypes {
  GetTheme = '[Theme] Get Theme',
  SetDarkTheme = '[Theme] Set Dark Theme',
  SetLightTheme = '[Theme] Set Light Theme'
};

export type ThemeType = 'dark' | 'light';

export class GetThemeAction {
  readonly type = ThemeActionTypes.GetTheme;
}

export class SetDarkThemeAction {
  readonly type = ThemeActionTypes.SetDarkTheme;
  readonly theme: ThemeType = 'dark';
}

export class SetLightThemeAction {
  readonly type = ThemeActionTypes.SetLightTheme;
  readonly theme: ThemeType = 'light';
}

export type ThemeAction = GetThemeAction | SetDarkThemeAction | SetLightThemeAction;