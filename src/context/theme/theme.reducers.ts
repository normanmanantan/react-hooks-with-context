import { ThemeActionTypes, ThemeAction, ThemeType } from './theme.actions';

export interface IThemeState {
  theme: ThemeType;
};

export const themeInitialState: IThemeState = {
  theme: 'light'
};

export const ThemeReducer = (state = themeInitialState, action: ThemeAction) => {
  switch (action.type) {
    case ThemeActionTypes.GetTheme:
      return {
        ...state
      };
    case ThemeActionTypes.SetDarkTheme:
    case ThemeActionTypes.SetLightTheme:
      return {
        ...state,
        theme: action.theme
      };
    default:
      return state;
  }
}