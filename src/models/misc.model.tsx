export interface INavigator extends Navigator {
  standalone: boolean;
}